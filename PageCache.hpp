#pragma once
#include"Common.hpp"
#include"radix.hpp"
class PageCache
{
public:
	Span* _NewSpan(size_t numpage);

	Span* NewSpan(size_t numpage);

	//向系统申请numpage页内存挂到自由链表
	//void SystemAllocPage(size_t numpage);

	void ReleaseSpanToPageCache(Span* span);
	Span* GetIdToSpan(PAGE_ID id);

	static PageCache& GetPageCacheInstance()
	{
		static PageCache inst;
		return inst;
	}

private:

	PageCache()
	{}
	PageCache(const PageCache&) = delete;

	//维护一个以页大小为哈希映射的 自由链表
	SpanList _spanLists[MAX_PAGES];
	
	Radix<Span*> _idSpanRadix;

	std::mutex _mtx;
};


//实现
Span* PageCache::_NewSpan(size_t numpage)
{
	//_spanLists[numpage].Lock();
	if (!_spanLists[numpage].Empty())
	{
		Span* span = _spanLists[numpage].Begin();
		_spanLists[numpage].PopFront();
		return span;
	}

	for (size_t i = numpage + 1; i < MAX_PAGES; ++i)
	{
		if (!_spanLists[i].Empty())
		{
			// 分裂
			Span* span = _spanLists[i].Begin();
			_spanLists[i].PopFront();

			//从Span后面切一个大小合适的
			Span* splitspan = new Span;
			splitspan->_pageid = span->_pageid + span->_pagesize - numpage;
			splitspan->_pagesize = numpage;
			//这些内存要要更新映射关系
			for (PAGE_ID i = 0; i < numpage; ++i)
			{
				//cout << "更新映射" << splitspan->_pageid + i << endl;
				_idSpanRadix.insert(splitspan->_pageid + i , splitspan);
			}

			span->_pagesize -= numpage;

			_spanLists[span->_pagesize].PushFront(span);

			return splitspan;
		}
	}

	void* ptr = SystemAlloc(MAX_PAGES - 1);

	Span* bigspan = new Span;
	//根据地址，计算出页号
	bigspan->_pageid = (PAGE_ID)ptr >> PAGE_SHITF;
	bigspan->_pagesize = MAX_PAGES - 1;

	for (PAGE_ID i = 0; i < bigspan->_pagesize; ++i)
	{
		//_idSpanRadix.insert(std::make_pair(bigspan->_pageid + i, bigspan));
		//cout << "插入" << bigspan->_pageid + i << endl;
		_idSpanRadix.insert(bigspan->_pageid + i,bigspan);
	}

	_spanLists[bigspan->_pagesize].PushFront(bigspan);

	return _NewSpan(numpage);
}

Span* PageCache::NewSpan(size_t numpage)
{
	_mtx.lock();
	Span* span = _NewSpan(numpage);
	_mtx.unlock();
	return span;
}

void PageCache::ReleaseSpanToPageCache(Span* span)
{
	// 向前合并
	while (1)
	{
		PAGE_ID prevPageId = span->_pageid - 1;
		auto pit = _idSpanRadix.find(prevPageId);
		// 前面的页不存在
		if (pit == nullptr)
		{
			break;
		}

		
		Span* prevSpan = pit->value;
		// 说明前一个也还在使用中，不能合并
		if (prevSpan->_usecount != 0)
		{
			break;
		}

		if (span->_pagesize + prevSpan->_pagesize >= MAX_PAGES)
		{
			break;
		}
		// 合并
		span->_pageid = prevSpan->_pageid;
		span->_pagesize += prevSpan->_pagesize;
		for (PAGE_ID i = 0; i < prevSpan->_pagesize; ++i)
		{
			//cout << "合并" << prevSpan->_pageid + i << endl;
			_idSpanRadix.insert(prevSpan->_pageid + i, span);
		}


		_spanLists[prevSpan->_pagesize].Erase(prevSpan);
		delete prevSpan;
	}


	// 向后合并
	while (1)
	{
		PAGE_ID nextPageId = span->_pageid + span->_pagesize;
		auto nextIt = _idSpanRadix.find(nextPageId);
		if (nextIt == nullptr)
		{
			break;
		}

		Span* nextSpan = nextIt->value;
		if (nextSpan->_usecount != 0)
		{
			break;
		}

		if (span->_pagesize + nextSpan->_pagesize >= MAX_PAGES)
		{
			break;
		}
		span->_pagesize += nextSpan->_pagesize;
		for (PAGE_ID i = 0; i < nextSpan->_pagesize; ++i)
		{
			//cout << "向后合并" << nextSpan->_pageid + i << endl;
			_idSpanRadix.insert(nextSpan->_pageid + i, span);
		}

		_spanLists[nextSpan->_pagesize].Erase(nextSpan);
		delete nextSpan;
	}

	_spanLists[span->_pagesize].PushFront(span);
}

Span* PageCache::GetIdToSpan(PAGE_ID id)
{
	//std::unordered_map<PAGE_ID, Span*>::iterator it = _idSpanRadix.find(id);
	//std::map<PAGE_ID, Span*>::iterator it = _idSpanRadix.find(id);
	auto it = _idSpanRadix.find(id);
	if (it != nullptr)
	{
		return it->value;
	}
	else
	{
		return nullptr;
	}
}


