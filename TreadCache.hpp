#include"Common.hpp"
#include"CentralCache.hpp"
class ThreadCache
{
public:
	//申请内存和释放内存
	void* Allocte(size_t size);
	void Deallocte(void* ptr, size_t size);

	//从中心缓存获取对象
	void* FetchFromCentralCache(size_t index);

	//如果自由链表超过一定长度就要释放给中心缓存
	void ListTooLong(FreeList& freeList, size_t num, size_t size);
private:
	//维护一个内存链表数组 链表是特殊链表 可以通过过数组下标获得对应大小内存
	FreeList _freeLists[NFREE_LIST];
};

//局部线程TSL 在全局里它值为空 在每个线程中为其申请空间。
_declspec(thread) static ThreadCache* pThreadCache = nullptr;


#define _CRT_SECURE_NO_WARNINGS 1

//实现

void* ThreadCache::Allocte(size_t size)
{
	//根据大小进行对齐找到对应下标位置
	size_t index = SizeClass::ListIndex(size);
	FreeList& freeList = _freeLists[index];
	if (!freeList.Empty())
	{
		return freeList.Pop();
	}
	else
	{
		return FetchFromCentralCache(SizeClass::RoundUp(size));
	}
}

void ThreadCache::Deallocte(void* ptr, size_t size)
{
	size_t index = SizeClass::ListIndex(size);
	FreeList& freeList = _freeLists[index];
	freeList.Push(ptr);

	// 对象个数满足一定条件内存大小
	size_t num = SizeClass::NumMoveSize(size);
	if (freeList.Num() >= num)
	{
		ListTooLong(freeList, num, size);
	}
}

void ThreadCache::ListTooLong(FreeList& freeList, size_t num, size_t size)
{
	void* start = nullptr, * end = nullptr;
	freeList.PopRange(start, end, num);

	NextObj(end) = nullptr;
	CentralCache::GetCentralCacheInstance().ReleseListToSpans(start, size);
}

// 从中心缓存获取span
void* ThreadCache::FetchFromCentralCache(size_t size)
{
	size_t num = SizeClass::NumMoveSize(size);

	void* start = nullptr, * end = nullptr;
	size_t actualNum = CentralCache::GetCentralCacheInstance().FetchRangeObj(start, end, num, size);

	if (actualNum == 1)
	{
		return start;
	}
	else
	{
		size_t index = SizeClass::ListIndex(size);
		FreeList& list = _freeLists[index];
		list.PushRange(NextObj(start), end, actualNum - 1);

		return start;
	}
}
